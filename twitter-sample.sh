#!/bin/bash

# Inleiding Wetenschappelijk Onderzoek

# Kai Bruijn, s3204766

echo 'The number of tweets in the sample is: ' 

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | wc -l

echo 'The number of unique tweets in the sample is: '

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | wc -l

echo 'The number of retweets in the sample is: '

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep '^RT' | wc -l

echo 'The first 20 unique tweets in the sample that are not retweets are: '

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -v '^RT' | head -20
