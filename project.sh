#!/bin/bash

echo 'The number of tweets by men:'

zless /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab user text | get-gender.py | grep -v 'RT' | grep -v '@' | grep -w 'male'
