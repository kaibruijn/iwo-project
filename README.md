# README #

This is a git repository for Inleiding Wetenschappelijk Onderzoek.
It contains a shellscript 'twittersample.sh' that accesses the in-house Twitter corpus.
The shell script accesses the twitter file from 1 March 2017 12:00 and analyzes the tweets of this specific time, answering the following questions: 

- How many tweets are in the sample?

- How many unique tweets are in the sample?

- How many retweets are in the sample (out of the unique tweets)?

In the end the script shows the first 20 unique tweets in the sample that are not retweets.
